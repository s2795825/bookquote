package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.logging.Handler;

public class Quoter {
    private HashMap<String,Double> priceMap = new HashMap<>();

    public Quoter() {
        priceMap.put("1",10.0);
        priceMap.put("2",45.0);
        priceMap.put("3",20.0);
        priceMap.put("4",35.0);
        priceMap.put("5",50.0);
    }

    public double getBookPrice(String isbn) {
        Double price = priceMap.get(isbn);
        if (price != null) {
            return price;
        } else {
            return 0;
        }
    }
}
